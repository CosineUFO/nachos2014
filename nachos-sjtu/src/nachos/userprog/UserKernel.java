package nachos.userprog;

import nachos.machine.*;
import nachos.threads.*;

import java.util.LinkedList;

/**
 * A kernel that can support multiple user processes.
 */
public class UserKernel extends ThreadedKernel {
    /**
     * Allocate a new user kernel.
     */
    public UserKernel() {
        super();
    }

    /**
     * Initialize this kernel. Creates a synchronized console and sets the
     * processor's exception handler.
     */
    public void initialize(String[] args) {
        super.initialize(args);

        console = new SynchConsole(Machine.console());

        Machine.processor().setExceptionHandler(new Runnable() {
            public void run() {
                exceptionHandler();
            }
        });

        pageLock = new Lock();
        processMapLock = new Lock();
        pidLock = new Lock();

        int numPhysPages = Machine.processor().getNumPhysPages();
        freePages = new LinkedList<Integer>();
        for (int i = 0; i < numPhysPages; ++i)
            freePages.add(new Integer(i));
    }

    /**
     * Test the console device.
     */
    public void selfTest() {
        super.selfTest();
    }

    /**
     * Returns the current process.
     * 
     * @return the current process, or <tt>null</tt> if no process is current.
     */
    public static UserProcess currentProcess() {
        if (!(KThread.currentThread() instanceof UThread))
            return null;

        return ((UThread) KThread.currentThread()).process;
    }

    /**
     * The exception handler. This handler is called by the processor whenever a
     * user instruction causes a processor exception.
     * 
     * <p>
     * When the exception handler is invoked, interrupts are enabled, and the
     * processor's cause register contains an integer identifying the cause of
     * the exception (see the <tt>exceptionZZZ</tt> constants in the
     * <tt>Processor</tt> class). If the exception involves a bad virtual
     * address (e.g. page fault, TLB miss, read-only, bus error, or address
     * error), the processor's BadVAddr register identifies the virtual address
     * that caused the exception.
     */
    public void exceptionHandler() {
        Lib.assertTrue(KThread.currentThread() instanceof UThread);

        UserProcess process = ((UThread) KThread.currentThread()).process;
        int cause = Machine.processor().readRegister(Processor.regCause);
        process.handleException(cause);
    }

    /**
     * Start running user programs, by creating a process and running a shell
     * program in it. The name of the shell program it must run is returned by
     * <tt>Machine.getShellProgramName()</tt>.
     * 
     * @see nachos.machine.Machine#getShellProgramName
     */
    public void run() {
        super.run();

        UserProcess process = UserProcess.newUserProcess();
        root = process;

        String shellProgram = Machine.getShellProgramName();
        Lib.assertTrue(process.execute(shellProgram, new String[] {}));

        KThread.finish();
    }

    protected UserProcess root;

    /**
     * Terminate this kernel. Never returns.
     */
    public void terminate() {
        super.terminate();
    }

    public static int allocPage() {
        int page = -1;
        pageLock.acquire();
        if (freePages.size() > 0)
            page = freePages.removeFirst();
        pageLock.release();
        return page;
    }

    public static void freePage(int ppn) {
    	pageLock.acquire();
        freePages.add(ppn);
        pageLock.release();
    }
    
    public static int getNextPid() {
    	int retval;
    	pidLock.acquire();
    	retval = ++pidCounter;
    	pidLock.release();
    	return retval;
    }

    public static UserProcess getProcess(int pid) {
    	return processMap.get(pid);
    }
    
    public static UserProcess registerProcess(int pid, UserProcess process) {
    	UserProcess insertedProcess;
    	processMapLock.acquire();
    	insertedProcess = processMap.put(pid, process);
    	processMapLock.release();
    	return insertedProcess;
    }        

    /** Globally accessible reference to the synchronized console. */
    public static SynchConsole console;

    private static Lock pageLock;
    private static Lock processMapLock;
    private static Lock pidLock;
    private static LinkedList<Integer> freePages;
    private static int pidCounter = 0;
    private static java.util.HashMap<Integer, UserProcess> processMap = new java.util.HashMap<Integer, UserProcess>();
    public static int runningProcessNum = 0;
}
