package nachos.threads;

import nachos.ag.BoatGrader;

public class Boat {
	static BoatGrader bg;

	public static void selfTest() {
		BoatGrader b = new BoatGrader();

		System.out.println("\n ***Testing Boats with only 2 children***");
		begin(0, 2, b);

		// System.out.println("\n ***Testing Boats with 2 children, 1 adult***");
		// begin(1, 2, b);

		// System.out.println("\n ***Testing Boats with 3 children, 3 adults***");
		// begin(3, 3, b);
	}

	public static void begin(int adults, int children, BoatGrader b) {
		// Store the externally generated autograder in a class
		// variable to be accessible by children.
		bg = b;

		// Instantiate global variables here

		// Create threads here. See section 3.4 of the Nachos for Java
		// Walkthrough linked from the projects page.
		lock = new Lock();
		condition = new Condition2(lock);
		adultsAtOahu = adults;
		childrenAtOahu = children;
		childrenAtMolokai = 0;
		boatLocation = OAHU;
		needPilot = false;

		java.util.List<KThread> threads = new java.util.LinkedList<KThread>();

		for (int i = 0; i < adults; i++) {
			KThread thread = new KThread(new Runnable() {
				public void run() {
					AdultItinerary();
				}
			});
			thread.setName("Adult" + i);
			thread.fork();
			threads.add(thread);
		}
		
		for (int i = 0; i < children; i++) {
			KThread thread = new KThread(new Runnable() {
				public void run() {
					ChildItinerary();
				}
			});
			thread.setName("Child" + i);
			thread.fork();
			threads.add(thread);
		}		

		// make sure judging thread is waiting
		for (KThread thread : threads)
			thread.join();
	}

	static void AdultItinerary() {
		/*
		 * This is where you should put your solutions. Make calls to the
		 * BoatGrader to show that it is synchronized. For example:
		 * bg.AdultRowToMolokai(); indicates that an adult has rowed the boat
		 * across to Molokai
		 */

		lock.acquire();

		while (boatLocation == MOLOKAI || childrenAtMolokai == 0)
			condition.sleep();

		adultsAtOahu--;
		boatLocation = MOLOKAI;
		bg.AdultRowToMolokai();
		condition.wakeAll();

		lock.release();
	}

	static void ChildItinerary() {
		int location = OAHU;
		lock.acquire();
		while (adultsAtOahu + childrenAtOahu > 0) {
			if (boatLocation != location) {
				condition.sleep();
			} else if (location == MOLOKAI) {
				childrenAtMolokai--;
				childrenAtOahu++;
				boatLocation = OAHU;
				location = OAHU;
				bg.ChildRowToOahu();
				condition.wakeAll();
			} else if (needPilot) {
				needPilot = false;
				location = MOLOKAI;
				boatLocation = MOLOKAI;
				childrenAtOahu -= 2;
				childrenAtMolokai += 2;
				bg.ChildRideToMolokai();
				condition.wakeAll();
			} else if (childrenAtOahu > 1) {
				needPilot = true;
				location = MOLOKAI;
				bg.ChildRowToMolokai();
			} else if(adultsAtOahu == 0) {
				childrenAtMolokai--;
				childrenAtOahu++;
				boatLocation = MOLOKAI;
				location = MOLOKAI;
				bg.ChildRowToMolokai();
			} else
				condition.sleep();					
		}
		lock.release();
	}


	static private Lock lock;
	static private Condition2 condition;
	static private int adultsAtOahu, childrenAtOahu, childrenAtMolokai;
	static private int boatLocation;
	private static boolean needPilot;
	private static final int OAHU = 0, MOLOKAI = 1;	
	
}
