package nachos.vm;

import java.util.HashMap;

import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.TranslationEntry;

public class PageTable {

	private static void setInfo(int ppn, TranslationEntry entry, Integer pid) {
		pagePid[ppn] = pid;
		pageEntry[ppn] = entry;
	}
	
	private static void setEntry(int pid, TranslationEntry entry, boolean isMerge) {
		Pair<Integer, Integer> pair = new Pair<Integer, Integer>(pid, entry.vpn);
		if (!pageTable.containsKey(pair))
			return;
		TranslationEntry oldEntry = pageTable.get(pair);
		TranslationEntry newEntry = new TranslationEntry(entry);
		if (isMerge) {
			newEntry.used |= oldEntry.used;
			newEntry.dirty |= oldEntry.dirty;
		}
		if (oldEntry.valid)
			setInfo(oldEntry.ppn, null, null);
		if (entry.valid)
			setInfo(entry.ppn, newEntry, pid);
		pageTable.put(pair, newEntry);
	}
	
	public static boolean add(int pid, TranslationEntry entry) {
		Pair<Integer, Integer> pair = new Pair<Integer, Integer>(pid, entry.vpn);
		if (pageTable.containsKey(pair))
			return false;
		pageTable.put(pair, entry);
		if (entry.valid)
			setInfo(entry.ppn, entry, pid);
		return true;
	}

	public static TranslationEntry remove(int pid, int vpn) {
		TranslationEntry entry = pageTable.remove(new Pair<Integer, Integer>(pid, vpn));
		if (entry != null && entry.valid)
			setInfo(entry.ppn, null, null);
		return entry;
	}

	public static Pair<TranslationEntry, Integer> getKilled() {
        int index = 0;
        do {
            index = Lib.random(pageEntry.length);
        } while (pageEntry[index] == null || pageEntry[index].valid == false);

        return new Pair<TranslationEntry, Integer>(pageEntry[index], pagePid[index]);
	}

	public static TranslationEntry get(int pid, int vpn) {
		Pair<Integer, Integer> pair = new Pair<Integer, Integer>(pid, vpn);
		if (!pageTable.containsKey(pair))
			return null;
		return new TranslationEntry(pageTable.get(pair));
	}

	public static void set(int pid, TranslationEntry entry) {
		setEntry(pid, entry, false);
	}

	public static void merge(int pid, TranslationEntry entry) {
		setEntry(pid, entry, true);
	}

	private static HashMap<Pair<Integer, Integer>, TranslationEntry> pageTable = new HashMap<Pair<Integer, Integer>, TranslationEntry>();
	private static TranslationEntry[] pageEntry = new TranslationEntry[Machine.processor().getNumPhysPages()];
	private static Integer[] pagePid = new Integer[Machine.processor().getNumPhysPages()];
	
	protected static final char dbgVM = 'v';
}
