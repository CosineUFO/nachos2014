package nachos.vm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import nachos.machine.OpenFile;
import nachos.machine.Processor;
import nachos.threads.ThreadedKernel;

public class Swap {

	public static void close() {
		if (swapFile == null)
			return;
		swapFile.close();
		ThreadedKernel.fileSystem.remove(swapName);
		swapFile = null;
	}	
	
	public static byte[] read(int pid, int vpn) {
		byte[] ret = new byte[pageSize];
		int pos = find(pid, vpn);
		if (pos == -1)
			return ret;
		if (swapFile.read(pos * pageSize, ret, 0, pageSize) == -1)
			ret = new byte[pageSize];
		return ret;
	}

	public static int write(int pid, int vpn, byte[] page, int offset) {
		int pos = alloc(pid, vpn);
		if (pos == -1)
			return 0;
		swapFile.write(pos * pageSize, page, offset, pageSize);
		return pos;
	}	
	
	public static void add(int pid, int vpn) {
		unallocSet.add(new Pair<Integer, Integer>(pid, vpn));
	}

	public static void remove(int pid, int vpn) {
		if (find(pid, vpn) == -1)
			return;
		hole.add(swapTable.remove(new Pair<Integer, Integer>(pid, vpn)));
	}

	private static int find(int pid, int vpn) {
		Integer ret = swapTable.get(new Pair<Integer, Integer>(pid, vpn));
		return ret == null ? -1 : ret;
	}

	private static int alloc(int pid, int vpn) {
		Pair<Integer, Integer> ip = new Pair<Integer, Integer>(pid, vpn);
		if (!unallocSet.contains(ip))
			return find(pid, vpn);

		unallocSet.remove(ip);
		if (hole.size() == 0)
			hole.add(swapTable.size());
		
		int index = hole.poll();
		swapTable.put(new Pair<Integer, Integer>(pid, vpn), index);
		return index;
	}

	protected final static char dbgVM = 'v';
	private final static String swapName = "/SWAP";

	private static HashSet<Pair<Integer, Integer>> unallocSet = new HashSet<Pair<Integer, Integer>>();
	private static HashMap<Pair<Integer, Integer>, Integer> swapTable = new HashMap<Pair<Integer, Integer>, Integer>();
	private static LinkedList<Integer> hole = new LinkedList<Integer>();
	private static OpenFile swapFile = ThreadedKernel.fileSystem.open(swapName, true);
	private static int pageSize = Processor.pageSize;


}
