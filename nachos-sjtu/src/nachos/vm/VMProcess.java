package nachos.vm;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import nachos.machine.CoffSection;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.machine.TranslationEntry;
import nachos.threads.Lock;
import nachos.userprog.UserProcess;

/**
 * A <tt>UserProcess</tt> that supports demand-paging.
 */
public class VMProcess extends UserProcess {
	/**
	 * Allocate a new process.
	 */
	public VMProcess() {
		super();
		for (int i = 0; i < Machine.processor().getTLBSize(); ++i)
			tlb[i] = new TranslationEntry(0, 0, false, false, false, false);
	}

	protected boolean allocPages(int vpn, int nPages, boolean readOnly) {
		for (int i = 0; i < nPages; ++i) {
			TranslationEntry entry = new TranslationEntry(vpn + i, 0, false,
					readOnly, false, false);
			PageTable.add(pid, entry);
			Swap.add(pid, vpn + i);
			page.add(vpn + i);
		}
		numPages += nPages;
		return true;
	}

	protected TranslationEntry findPageTable(int vpn) {
		return PageTable.get(pid, vpn);
	}

	public int readVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		vmLock.acquire();
		swap(Processor.pageFromAddress(vaddr));
		TranslationEntry entry = translate(vaddr);
		entry.used = true;
		PageTable.set(pid, entry);
		vmLock.release();
		return super.readVirtualMemory(vaddr, data, offset, length);
	}

	public int writeVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		vmLock.acquire();
		swap(Processor.pageFromAddress(vaddr));
		TranslationEntry entry = translate(vaddr);
		entry.dirty = true;
		PageTable.set(pid, entry);
		vmLock.release();
		return super.writeVirtualMemory(vaddr, data, offset, length);
	}

	protected int getFreePage() {
		int ppn = VMKernel.allocPage();
		if (ppn == -1) {
			Pair<TranslationEntry, Integer> killed = PageTable.getKilled();
			ppn = killed.first.ppn;
			int pid = killed.second;
			int vpn = killed.first.vpn;
			TranslationEntry entry = PageTable.get(pid, vpn);
			Processor processor = Machine.processor();
			for (int i = 0; i < processor.getTLBSize(); ++i) {
				TranslationEntry tlbEntry = processor.readTLBEntry(i);
				if (tlbEntry.valid && tlbEntry.vpn == entry.vpn
						&& tlbEntry.ppn == entry.ppn) {
					PageTable.merge(pid, tlbEntry);
					entry = PageTable.get(pid, vpn);
					tlbEntry.valid = false;
					processor.writeTLBEntry(i, tlbEntry);
					break;
				}
			}
			if (entry.dirty) {
				byte[] memory = Machine.processor().getMemory();
				Swap.write(pid, entry.vpn, memory,
						entry.ppn * pageSize);
			}
			entry.valid = false;
			PageTable.set(pid, entry);			
		}
		return ppn;
	}

	protected int swap(int vpn) {
		TranslationEntry entry = findPageTable(vpn);
		if (entry.valid)
			return entry.ppn;
		int ppn = getFreePage();
		entry = findPageTable(vpn);
		boolean dirty = false;
		
		if (lazySection.containsKey(vpn)) {
			loadLazySection(vpn, ppn);
			dirty = true;
		} else {
			byte[] page = Swap.read(pid, vpn);
			byte[] memory = Machine.processor().getMemory();
			System.arraycopy(page, 0, memory, ppn * pageSize, pageSize);
			dirty = false;
		}
		TranslationEntry newEntry = new TranslationEntry(entry);
		newEntry.valid = true;
		newEntry.ppn = ppn;
		newEntry.dirty = newEntry.used = dirty;
		PageTable.set(pid, newEntry);
		return ppn;
	}


	protected void loadLazySection(int vpn, int ppn) {
		Pair<Integer, Integer> ip = lazySection.remove(vpn);
		if (ip == null)
			return;
		coff.getSection(ip.first).loadPage(ip.second, ppn);
	}

	protected void releaseResource() {
		for (Integer vpn : page) {
			vmLock.acquire();
			TranslationEntry entry = PageTable.remove(pid, vpn);
			if (entry.valid)
				VMKernel.freePage(entry.ppn);
			Swap.remove(pid, vpn);
			vmLock.release();
		}
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
		Lib.debug(dbgVM, "save state " + pid);
		Processor processor = Machine.processor();
		for (int i = 0; i < processor.getTLBSize(); ++i) {
			tlb[i] = processor.readTLBEntry(i);
			if (tlb[i].valid)
				PageTable.merge(pid, processor.readTLBEntry(i));
		}
		super.saveState();
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {
		Lib.debug(dbgVM, "restore state " + pid);

		Processor processor = Machine.processor();
		for (int i = 0; i < processor.getTLBSize(); ++i) {
			TranslationEntry t = invalidEntry();
			if (tlb[i].valid) {
				TranslationEntry entry = findPageTable(tlb[i].vpn);
				if (entry != null && entry.valid)
					t = tlb[i];
			}
			processor.writeTLBEntry(i, t);
		}
	}

	/**
	 * Initializes page tables for this process so that the executable can be
	 * demand-paged.
	 * 
	 * @return <tt>true</tt> if successful.
	 */
	protected boolean loadSections() {
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection coffSection = coff.getSection(s);
			for (int i = 0; i < coffSection.getLength(); i++) {
				int vpn = coffSection.getFirstVPN() + i;
				lazySection.put(vpn, new Pair<Integer, Integer>(s, i));
			}
		}
		return true;
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {
		super.unloadSections();
	}

	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 */
	public void handleException(int cause) {
		Processor processor = Machine.processor();

		switch (cause) {
		case Processor.exceptionTLBMiss:
			vmLock.acquire();
			int vaddr = processor.readRegister(Processor.regBadVAddr);
			if (!handleTLBMiss(vaddr)) {
				vmLock.release();
				exitWithException();
			} else
				vmLock.release();
			break;
		default:
			super.handleException(cause);
			break;
		}
	}

	protected boolean handleTLBMiss(int vaddr) {
		TranslationEntry targetEntry = translate(vaddr);
		if (targetEntry == null)
			return false;
		if (!targetEntry.valid) {
			swap(Processor.pageFromAddress(vaddr));
			targetEntry = translate(vaddr);
		}
		int killedIndex = Lib.random(Machine.processor().getTLBSize());
		for (int i = 0; i < Machine.processor().getTLBSize(); ++i)
			if (Machine.processor().readTLBEntry(i).valid == false) {
				killedIndex = i;
				break;
			}
		TranslationEntry subEntry = Machine.processor().readTLBEntry(killedIndex);
		if (subEntry.valid)
			PageTable.merge(pid, subEntry);
		Machine.processor().writeTLBEntry(killedIndex, targetEntry);
		return true;
	}

	private TranslationEntry invalidEntry() {
		return new TranslationEntry(0, 0, false, false, false, false);
	}

	protected Map<Integer, Pair<Integer, Integer>> lazySection = new HashMap<Integer, Pair<Integer, Integer>>();
	protected LinkedList<Integer> page = new LinkedList<Integer>();
	protected TranslationEntry[] tlb = new TranslationEntry[Machine.processor().getTLBSize()];

	protected static Lock vmLock = new Lock();
	protected static final int pageSize = Processor.pageSize;
	protected static final char dbgVM = 'v';
}
