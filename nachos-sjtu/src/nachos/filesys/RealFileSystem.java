package nachos.filesys;

import java.util.HashMap;
import java.util.HashSet;

import nachos.machine.FileSystem;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.OpenFile;

/**
 * RealFileSystem provide necessary methods for filesystem syscall.
 * The FileSystem interface already define two basic methods, you should implement your own to adapt to your task.
 * 
 * @author starforever
 */
public class RealFileSystem implements FileSystem
{
  /** the free list */
  private FreeList free_list;
  
  /** the root folder */
  private Folder root_folder;
  
  private HashSet<Integer> remove = new HashSet<Integer>(); 
  
  private HashMap<Integer, INode> cache = new HashMap<Integer, INode>();
  private HashMap<Integer, File> fCache = new HashMap<Integer, File>();
 
  /**
   * initialize the file system
   * 
   * @param format
   *          whether to format the file system
   */
  public void init (boolean format)
  {
    
      INode inode_free_list = new INode(FreeList.STATIC_ADDR);
      free_list = new FreeList(inode_free_list);
      free_list.create();
      
      INode inode_root_folder = new INode(Folder.STATIC_ADDR);
      root_folder = new Folder(inode_root_folder);
      root_folder.create();
      
      if (format) {
    	  importStub();
      }
  }
  
  public void finish ()
  {
    root_folder.save();
    free_list.save();
  }
  
  /** import from stub filesystem */
  private void importStub ()
  {
    FileSystem stubFS = Machine.stubFileSystem();
    FileSystem realFS = FilesysKernel.realFileSystem;
    String[] file_list = Machine.stubFileList();
    for (int i = 0; i < file_list.length; ++i)
    {
      if (!file_list[i].endsWith(".coff"))
        continue;
      OpenFile src = stubFS.open(file_list[i], false);
      if (src == null)
      {
        continue;
      }
      OpenFile dst = realFS.open("/" + file_list[i], true);
      int size = src.length();
      byte[] buffer = new byte[size];
      src.read(0, buffer, 0, size);
      dst.write(0, buffer, 0, size);
      src.close();
      dst.close();
    }
  }
  
  /** get the only free list of the file system */
  public FreeList getFreeList ()
  {
    return free_list;
  }
  
  /** get the only root folder of the file system */
  public Folder getRootFolder ()
  {
    return root_folder;
  }
  
  public OpenFile open (String name, boolean create)
  {
	  String path = getPath(name);
	  if (path == null)
		  return null;
	  Folder folder = (Folder) getFile(path);
	  if (folder == null)
		  return null;
	  String filename = getName(name);
	  if (name == null)
		  return null;
	  Integer addr = folder.find(filename);
	  File file = null;
	  if (addr == null) {
		  if (!create)
			  return null;
		  file = newFile(folder, filename, INode.TYPE_FILE);
	  }
	  else
		  file = getFile(addr);
	  if (file.inode.isFolder())
		  return null;
	  if (file.inode.isLink())
		  return open(((SymLink) file).getPath(), create);
	  file.inode.use_count++;
	  return file;
  }
  
  public boolean remove (String name)
  {
	  String path = getPath(name);
	  if (path == null)
		  return false;
	  String filename = getName(name);
	  if (filename == null)
		  return false;
	  Folder folder = (Folder) getFile(path);
	  if (folder == null)
		  return false;
	  Integer addr = folder.removeEntry(filename);
	  if (addr == null)
		  return false;
	  File file = getFile(addr);
	  INode inode = file.inode;
	  if (inode.isFolder())
		  return false;
	  inode.link_count--;
	  if (inode.link_count == 0 && inode.isLink()) {
		  fCache.remove(file);
	  }
	  doRemove(inode);
	  return true;
  }
  
  public boolean doRemove(INode inode) {
	  Lib.assertTrue(inode.link_count >= 0 && inode.use_count >= 0);
	  if (inode.link_count == 0 && inode.use_count == 0) {
		  remove.remove(inode.addr);
		  cache.remove(inode.addr);
		  inode.free();
		  return true;
	  }
	  else {
		  remove.add(inode.addr);
		  return false;
	  }
  }
  
  public void close (INode inode) {
	  --inode.use_count;
	  if (remove.contains(inode.addr))
		  doRemove(inode);
  }
  
  public boolean createFolder (String name) {
	  String path = getPath(name);
	  if (path == null)
		  return false;
	  String filename = getName(name);
	  if (filename == null)
		  return false;
	  Folder pFolder = (Folder) getFile(path);
	  if (pFolder == null)
		  return false;
	  if (pFolder.contains(filename))
		  return false;
	  newFile(pFolder, filename, INode.TYPE_FOLDER);
      return true;
  }
  
  public boolean removeFolder (String name)
  {
	  Folder folder = (Folder) getFile(name);
	  if (folder == null)
		  return false;
	  if (!folder.isEmpty())
		  return false;
	  String path = getPath(name);
	  if (path == null)
		  return false;
	  String filename = getName(name);
	  if (filename == null)
		  return false;
	  Folder pFolder = (Folder) getFile(path);
	  if (pFolder == null)
		  return false;
	  if (!pFolder.contains(filename))
		  return false;
	  fCache.remove(folder.inode.addr);
	  pFolder.removeEntry(filename);
	  folder.inode.link_count--;
	  doRemove(folder.inode);
	  return true;
  }
  
  public String[] readDir (String name)
  {
    //TODO implement this
    return null;
  }
  
  public FileStat getStat (String name)
  {
    //TODO implement this
    return null;
  }
  
  public boolean createLink (String src, String dst)
  {
	  String srcPath = getPath(src);
	  if (srcPath == null)
		  return false;
	  String srcName = getName(src);
	  if (srcName == null)
		  return false;
	  String dstPath = getPath(dst);
	  if (dstPath == null)
		  return false;
	  String dstName = getName(dst);
	  if (dstName == null)
		  return false;
	  Folder srcFolder = (Folder) getFile(srcPath);
	  if (srcFolder == null)
		  return false;
	  Folder dstFolder = (Folder) getFile(dstPath);
	  if (dstFolder == null)
		  return false;
	  Integer srcAddr = srcFolder.find(srcName);
	  if (srcAddr == null)
		  return false;
	  if (dstFolder.contains(dstName))
		  return false;
	  dstFolder.addEntry(dstName, srcAddr);
	  getFile(srcAddr).inode.link_count++;
	  return true;
  }
  
  public boolean createSymlink (String src, String dst)
  {
	  String dstPath = getPath(dst);
	  if (dstPath == null)
		  return false;
	  String dstName = getName(dst);
	  if (dstName == null)
		  return false;
	  Folder dstFolder = (Folder) getFile(dstPath);
	  if (dstFolder == null)
		  return false;
	  if (dstFolder.contains(dstName))
		  return false;
	  SymLink file = (SymLink) newFile(dstFolder, dstName, INode.TYPE_SYMLINK);
	  file.setPath(src);
    return true;
  }
  
  public int getFreeSize()
  {
    //TODO implement this
    return 0;
  }
  
  private String getPathOrName(String fileName, boolean isPath) {
		if (!fileName.startsWith("/"))
			return null;
		if (fileName.endsWith("/"))
			fileName = fileName.substring(0, fileName.length() - 1);
		int pos = fileName.lastIndexOf("/");
		return isPath ? fileName.substring(0, pos + 1) : fileName
				.substring(pos + 1);
  }

  private String getPath(String fileName) {
	return getPathOrName(fileName, true);
  }    
  
  private String getName(String fileName) {
	return getPathOrName(fileName, false);
  }  

  protected File getFile(String fileName) {
		if (fileName.equals("/"))
			return root_folder;
		if (fileName.endsWith("/"))
			fileName = fileName.substring(0, fileName.length() - 1);		
		int pos = fileName.indexOf("/"), nextPos;
		Folder cur = root_folder;
		if (pos == -1)
			return null;
		for (;; pos = nextPos) {
			nextPos = fileName.indexOf("/", pos + 1);
			String entryName;
			if (nextPos != -1)
				entryName = fileName.substring(pos + 1, nextPos);
			else
				entryName = fileName.substring(pos + 1);
			Integer addr = cur.find(entryName);
			if (addr == null)
				return null;
			File entry = getFile(addr);
			if (entry.inode.isLink()) {
				// TODO
			} else {
				if (nextPos == -1)
					return entry;
				if (!entry.inode.isFolder())
					return null;
				cur = (Folder) entry;
			}
		}
	}
	
	File getFile(int addr) {
		if (fCache.containsKey(addr))
			return fCache.get(addr);
		INode inode = null;
		if (cache.containsKey(addr))
			inode = cache.get(addr);
		else {
			inode = new INode(addr);
			inode.load();
		}
		File file = null;
		if (inode.isFile())
			file = new File(inode);
		else if (inode.isFolder()) {
			file = new Folder(inode);
			((Folder) file).load();
			fCache.put(addr, file);
		}
		else if (inode.isLink()) {
			file = new SymLink(inode);
			((SymLink) file).load();
			fCache.put(addr, file);
		}
		cache.put(addr, inode);
		return file;
	}
	
	File newFile(Folder pFolder, String filename, int type) {
		int addr = free_list.allocate();
		File file = null;
		INode inode = new INode(addr);
		inode.file_type = type;
		if (inode.isFile())
			file = new File(inode);
		else if (inode.isFolder())
			file = new Folder(inode);
		else if (inode.isLink())
			file = new SymLink(inode);
		file.create();
		cache.put(addr, inode);
		pFolder.addEntry(filename, addr);
		if (inode.isFolder() || inode.isLink())
			fCache.put(addr, file);
		return file;
	}
	
	public int getSwapFileSectors() {
		return 0;
	}
	
}
