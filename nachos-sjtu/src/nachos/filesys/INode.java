package nachos.filesys;

import java.util.ArrayList;

import nachos.machine.Disk;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.SynchDisk;

/**
 * INode contains detail information about a file.
 * Most important among these is the list of sector numbers the file occupied, 
 * it's necessary to find all the pieces of the file in the filesystem.
 * 
 * @author starforever
 */
public class INode
{
  /** represent a system file (free list) */
  public static int TYPE_SYSTEM = 0;
  
  /** represent a folder */
  public static int TYPE_FOLDER = 1;
  
  /** represent a normal file */
  public static int TYPE_FILE = 2;
  
  /** represent a symbolic link file */
  public static int TYPE_SYMLINK = 3;
  
  /** size of the file in bytes */
  int file_size;
  
  /** the type of the file */
  int file_type;
  
  /** the number of programs that have access on the file */
  int use_count;
  
  /** the number of links on the file */
  int link_count;
  
  /** maintain all the sector numbers this file used in order */
  private ArrayList<Integer> sec_addr;
  
  /** the extended address */
  private ArrayList<Integer> ext_addr;
  
  int addr;
  
  SynchDisk disk = Machine.synchDisk();
  
  FreeList freeList = FilesysKernel.realFileSystem.getFreeList();
  
  public INode (int addr) {
	    file_size = 0;
	    file_type = TYPE_FILE;
	    use_count = 0;
	    link_count = 0;
	    sec_addr = new ArrayList<Integer>();
	    ext_addr = new ArrayList<Integer>();
  }

  /** get the sector number of a position in the file  */
  public int getSector (int pos)
  {
	  Lib.assertTrue(pos <= file_size);
    int secn = pos / Disk.SectorSize;
	return sec_addr.get(secn);
  }
  
  /** change the file size and adjust the content in the inode accordingly */
  public void setFileSize (int size)
  {
	  int secn = Lib.divRoundUp(size, Disk.SectorSize);
	  while (sec_addr.size() < secn) {
		  sec_addr.add(freeList.allocate());
	  }
	  while (sec_addr.size() > secn) {
		  freeList.deallocate(sec_addr.remove(sec_addr.size() - 1));
	  }
	  
	  int inode_size = getSize();
	  while (Lib.divRoundUp(inode_size, Disk.SectorSize) - 1 > ext_addr.size()) {
		  ext_addr.add(freeList.allocate());
		  inode_size += 4;
	  }
	  while (Lib.divRoundUp(inode_size, Disk.SectorSize) - 1 < ext_addr.size()) {
		  freeList.deallocate(ext_addr.remove(ext_addr.size() - 1));
		  inode_size -= 4;
	  }
	  
	  file_size = size;
  }
  
  /** free the disk space occupied by the file (including inode) */
  public void free ()
  {
	  for (int i : sec_addr)
		  freeList.deallocate(i);
	  for (int i : ext_addr)
		  freeList.deallocate(i);
	  sec_addr.clear();
	  ext_addr.clear();
  }
  
  /** load inode content from the disk */
  public void load ()
  {
	  byte[] data = new byte[Disk.SectorSize];
	  disk.readSector(addr, data, 0);
	  file_size = Lib.bytesToInt(data, 0);
	  file_type = Lib.bytesToInt(data, 4);
	  use_count = Lib.bytesToInt(data, 8);
	  link_count = Lib.bytesToInt(data, 12);
	  int ext_size = Lib.bytesToInt(data, 16);
	  int sec_size = Lib.bytesToInt(data, 20);
	  int offset = 24;
	  int extn = 0;
	  while (ext_size > 0) {
		  int secn = Lib.bytesToInt(data, offset);
		  ext_addr.add(secn);
		  offset += 4;
		  --ext_size;
		  if (offset >= Disk.SectorSize) {
			  disk.readSector(ext_addr.get(extn), data, 0);
			  ++extn;
			  offset = 0;
		  }
	  }
	  while (sec_size > 0) {
		  int secn = Lib.bytesToInt(data, offset);
		  sec_addr.add(secn);
		  offset += 4;
		  --sec_size;
		  if (offset >= Disk.SectorSize) {
			  disk.readSector(sec_addr.get(extn), data, 0);
			  ++extn;
			  offset = 0;
		  }
	  }
  }
  
  /** save inode content to the disk */
  public void save ()
  {
	  byte[] data = new byte[Disk.SectorSize];
	  Lib.bytesFromInt(data, 0, file_size);
	  Lib.bytesFromInt(data, 4, file_type);
	  Lib.bytesFromInt(data, 8, use_count);
	  Lib.bytesFromInt(data, 12, link_count);
	  Lib.bytesFromInt(data, 16, ext_addr.size());
	  Lib.bytesFromInt(data, 20, sec_addr.size());
	  int offset = 24;
	  int extn = -1;
	  for (int i = 0; i < ext_addr.size(); ++i, offset += 4) {
		  Lib.bytesFromInt(data, offset, ext_addr.get(i));
		  if (offset >= Disk.SectorSize) {
			  offset = 0;
			  disk.writeSector(extn >= 0 ? ext_addr.get(extn) : addr, data, 0);
			  ++extn;
		  }
	  }
	  for (int i = 0; i < sec_addr.size(); ++i, offset += 4) {
		  Lib.bytesFromInt(data, offset, sec_addr.get(i));	
		  if (offset >= Disk.SectorSize) {
			  offset = 0;
			  disk.writeSector(extn >= 0 ? ext_addr.get(extn) : addr, data, 0);
			  ++extn;
		  }		  
	  }
  }
  
  public void saveContent(byte[] data) {
	  int offset = 0;
	  for (int i = 0; i < sec_addr.size(); ++i) {
		  int secn = sec_addr.get(i);
		  disk.writeSector(secn, data, offset);
		  offset += Disk.SectorSize;
	  }
  }
  
  public byte[] loadContent() {
	  byte[] data = new byte[file_size];
	  int offset = 0;
	  for (int i = 0; i < sec_addr.size(); ++i) {
		  int secn = sec_addr.get(i);
		  disk.readSector(secn, data, offset);
		  offset += Disk.SectorSize;
	  }
	  return data;
  }

  private int getSize() {
	  return 20 + ((ext_addr.size() + sec_addr.size()) << 2);
  }
  
  public boolean isFolder() {
	  return file_type == TYPE_FOLDER;
  }
  
  public boolean isFile() {
	  return file_type == TYPE_FILE;
  }
  
  public boolean isLink() {
	  return file_type == TYPE_SYMLINK;
  }
  
}
