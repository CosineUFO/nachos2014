package nachos.filesys;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map.Entry;

import nachos.machine.Lib;

/**
 * Folder is a special type of file used to implement hierarchical filesystem.
 * It maintains a map from filename to the address of the file.
 * There's a special folder called root folder with pre-defined address.
 * It's the origin from where you traverse the entire filesystem.
 * 
 * @author starforever
 */
public class Folder extends File
{
  
	/** the static address for root folder */
	public static int STATIC_ADDR = 1;	
	
  /** mapping from filename to folder entry */
  private Hashtable<String, Integer> entry;
  
  public Folder (INode inode)
  {
    super(inode);
    entry = new Hashtable<String, Integer>();
  }
  
  public Integer find(String filename) {
	  return entry.get(filename);
  }
  
  public boolean contains (String filename) {
	  return entry.containsKey(filename);
  }
  
  public void addEntry (String filename, Integer addr) {
	  Lib.assertTrue(entry.put(filename, addr) == null);
	  inode.setFileSize(inode.file_size + 256 + 4);
  }
  
  /** remove an entry from the folder */
  public Integer removeEntry (String filename) {
	  Integer ret = entry.remove(filename);
	  Lib.assertTrue(ret != null);
	  inode.setFileSize(inode.file_size - 256 - 4);
	  return ret;
  }
  
  public boolean isEmpty() {
	  return entry.isEmpty();
  }
  
  public void create() {
	  super.create();
	  inode.file_type = INode.TYPE_FOLDER;
	  inode.setFileSize(4);
  }
  
  /** save the content of the folder to the disk */
  public void save ()
  {
	  byte[] data = new byte[inode.file_size];
	  Lib.bytesFromInt(data, 0, entry.size());
	  int offset = 4;
	  Iterator<Entry<String, Integer>> iter = entry.entrySet().iterator();
	  while (iter.hasNext()) {
		  Entry<String, Integer> entry = iter.next();
		  String key = entry.getKey();
		  int val = entry.getValue();
		  strToBytes(key, data, offset);
		  offset += 256;
		  Lib.bytesFromInt(data, offset, val);
		  offset += 4;
	  }
	  inode.saveContent(data);
  }
  
  /** load the content of the folder from the disk */
  public void load ()
  {
	  byte[] data = inode.loadContent();
	  int len = Lib.bytesToInt(data, 0);
	  int offset = 4;
	  for (int i = 0; i < len; ++i) {
		  String key = bytesToStr(data, offset);
		  offset += 256;
		  int value = Lib.bytesToInt(data, offset);
		  offset += 4;
		  entry.put(key, value);
	  }
  }
  
  private void strToBytes(String str, byte[] data, int offset) {
	  Lib.assertTrue(str.length() < 256);
	  for (int i = 0; i < str.length(); ++i) {
		  data[offset + i] = (byte) str.charAt(i);
	  }
	  data[offset + str.length()] = '\0';
  }
  
  private String bytesToStr(byte[] data, int offset) {
	  int i = 0;
	  char[] str = new char[256];
	  while (data[offset+i] != '\0') {
		  str[i] = (char) data[offset+i];
		  ++i;
		  Lib.assertTrue(i <= 256);
	  }
	  str[i] = '\0';
	  return new String(str);
  }
}
