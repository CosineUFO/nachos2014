package nachos.filesys;

import java.util.LinkedList;

import nachos.machine.Kernel;
import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.vm.VMProcess;

/**
 * FilesysProcess is used to handle syscall and exception through some callback methods.
 * 
 * @author starforever
 */
public class FilesysProcess extends VMProcess
{
  protected static final int SYSCALL_MKDIR = 14;
  protected static final int SYSCALL_RMDIR = 15;
  protected static final int SYSCALL_CHDIR = 16;
  protected static final int SYSCALL_GETCWD = 17;
  protected static final int SYSCALL_READDIR = 18;
  protected static final int SYSCALL_STAT = 19;
  protected static final int SYSCALL_LINK = 20;
  protected static final int SYSCALL_SYMLINK = 21;
  
  public int handleMKDIR (int addr) {
	  String name = readVirtualMemoryString(addr, 256);
	  return (realFS.createFolder(toAbsPath(name))) ? 0 : -1;
  }
  
  public int handleRMDIR (int addr) {
	  String name = readVirtualMemoryString(addr, 256);
	  return (realFS.removeFolder(toAbsPath(name))) ? 0 : -1;
  }
  
  public int handleCHDIR (int addr) {
	  String path = readVirtualMemoryString(addr, 256);
	  if (!path.endsWith("/"))
		  path += "/";
	  path = toAbsPath(path);
	  File file = realFS.getFile(path);
	  if (file == null || !file.inode.isFolder())
		  return -1;
	  kernel.changeCWD(path);
	  return 0;
  }  
  
  public int handleGETCWD (int addr, int size) {
	  String path = kernel.getCWD();
	  if (!path.equals("/") && path.endsWith("/"))
		  path = path.substring(0, path.length() - 1);	
	  byte[] data = path.getBytes();
	  if (data.length > size)
		  return -1;
	  writeVirtualMemory(addr, data);
	  return 0;
  }
  
  public int handleRDDIR (int dirAddr, int bufAddr, int size, int numSize) {
	  return 0;
  }    

  public int handleStat (int srcAddr, int dstAddr) {
	  return 0;
  }      
  
  public int handleLink (int srcAddr, int dstAddr) {
	  String src = readVirtualMemoryString(srcAddr, 256);
	  String dst = readVirtualMemoryString(dstAddr, 256);
	  realFS.createLink(toAbsPath(src), toAbsPath(dst));
	  return 0;
  }  

  public int handleSymLink (int srcAddr, int dstAddr) {
	  String src = readVirtualMemoryString(srcAddr, 256);
	  String dst = readVirtualMemoryString(dstAddr, 256);
	  realFS.createSymlink(toAbsPath(src), toAbsPath(dst));
	  return 0;
  }    
  
  public int handleSyscall (int syscall, int a0, int a1, int a2, int a3)
  {
    switch (syscall)
    {
      case SYSCALL_MKDIR:
      {
    	  return handleMKDIR(a0);
      }
        
      case SYSCALL_RMDIR:
      {
    	  return handleRMDIR(a0);
      }
        
      case SYSCALL_CHDIR:
      {
    	  return handleCHDIR(a0);
      }
        
      case SYSCALL_GETCWD:
      {
    	  return handleGETCWD(a0, a1);
      }
        
      case SYSCALL_READDIR:
      {
    	  return handleRDDIR(a0, a1, a2, a3);
      }
        
      case SYSCALL_STAT:
      {
    	  return handleStat(a0, a1);
      }
       
      case SYSCALL_LINK:
      {
    	  return handleLink(a0, a1);
      }
      
      case SYSCALL_SYMLINK:
      {
    	  return handleSymLink(a0, a1);
      }
      
      default:
        return super.handleSyscall(syscall, a0, a1, a2, a3);
    }
  }
  
  
  @Override
  public String toAbsPath(String s) {
      if (s.startsWith("/"))
          return convPath(s);
      else
          return convPath(kernel.getCWD() + s);
  }
  
  public String convPath(String s) {
	  String path = new String(s);
	  int pos = path.indexOf("/"), nextPos;
	  if (pos == -1)
		  return null;
	  LinkedList<String> pathLink = new LinkedList<String>();
	  do {
		  nextPos = path.indexOf("/", pos + 1);
		  String entryName;
		  if (nextPos != -1)
			  entryName = path.substring(pos + 1, nextPos);
		  else
			  entryName = path.substring(pos + 1);
		  if (entryName.equals(".."))
			  pathLink.removeLast();
		  else if (!entryName.equals("."))
			  pathLink.addLast(entryName);
		  pos = nextPos;
	  } while (pos != -1);
	  String ret = new String();
	  for (String i : pathLink) {
		  ret += '/' + i;
	  }
	  return ret;
  }
  
  private RealFileSystem realFS = FilesysKernel.realFileSystem;
  private FilesysKernel kernel = (FilesysKernel) Kernel.kernel;
}
