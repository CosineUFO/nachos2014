package nachos.filesys;

import java.util.LinkedList;

import nachos.machine.Disk;
import nachos.machine.Lib;
import nachos.machine.Machine;

/**
 * FreeList is a single special file used to manage free space of the filesystem.
 * It maintains a list of sector numbers to indicate those that are available to use.
 * When there's a need to allocate a new sector in the filesystem, call allocate().
 * And you should call deallocate() to free space at a appropriate time (eg. when a file is deleted) for reuse in the future.
 * 
 * @author starforever
 */
public class FreeList extends File
{
  /** the static address */
  public static int STATIC_ADDR = 0;
  
  /** maintain address of all the free sectors */
  private LinkedList<Integer> free_list;
  
  public FreeList (INode inode)
  {
    super(inode);
    free_list = new LinkedList<Integer>();
  }
  
  /** allocate a new sector in the disk */
  public int allocate ()
  {
	  return free_list.removeFirst();
  }
  
  /** deallocate a sector to be reused */
  public void deallocate (int sec)
  {
	  free_list.add(sec);
  }
  
  /** save the content of freelist to the disk */
  public void save ()
  {
	  byte[] data = new byte[Disk.SectorSize];
	  Lib.bytesFromInt(data, free_list.size(), 0);
	  int offset = 4;
	  for (int i = 0; i < free_list.size(); ++i, offset += 4) {
		  Lib.bytesFromInt(data, free_list.get(i), offset);
	  }
	  Machine.synchDisk().writeSector(STATIC_ADDR, data, 0);
  }
  
  /** load the content of freelist from the disk */
  public void load ()
  {
	  byte[] data = new byte[Disk.SectorSize];
	  Machine.synchDisk().readSector(STATIC_ADDR, data, 0);
	  int size = Lib.bytesToInt(data, 0);
	  int offset = 4;
	  for (int i = 0; i < size; ++i, offset += 4) {
		  free_list.add(Lib.bytesToInt(data, offset));
	  }
	  
  }
  
  public void create() {
	  for (int i = 0; i < Disk.NumSectors; ++i)
		  if (i != STATIC_ADDR && i != Folder.STATIC_ADDR)
			  free_list.add(i);
  }
}
