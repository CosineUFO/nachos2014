package nachos.filesys;

import nachos.machine.Disk;
import nachos.machine.Machine;
import nachos.machine.OpenFile;

/**
 * File provide some basic IO operations.
 * Each File is associated with an INode which stores the basic information for the file.
 * 
 * @author starforever
 */
public class File extends OpenFile
{
  INode inode;
  
  private int pos;
  
  public File (INode inode)
  {
    this.inode = inode;
    pos = 0;
  }
  
  public int length ()
  {
    return inode.file_size;
  }
  
  public void close ()
  {
	  FilesysKernel.realFileSystem.close(this.inode);
  }
  
  public void seek (int pos)
  {
    this.pos = pos;
  }
  
  public int tell ()
  {
    return pos;
  }
  
  public int read (byte[] buffer, int offset, int length)
  {
    int ret = read(pos, buffer, offset, length);
    pos += ret;
    return ret;
  }
  
  public int write (byte[] buffer, int offset, int length)
  {
    int ret = write(pos, buffer, offset, length);
    pos += ret;
    return ret;
  }
  
  public int read (int pos, byte[] buffer, int offset, int length)
  {
	  int nRead = 0;
	  length = Math.min(length, inode.file_size - pos);
	  if (pos % Disk.SectorSize != 0) {
		  int secn = inode.getSector(pos);
		  byte[] data = new byte[Disk.SectorSize];
		  Machine.synchDisk().readSector(secn, data, 0);
		  nRead = Math.min(length, Disk.SectorSize - pos % Disk.SectorSize);
		  System.arraycopy(data, pos % Disk.SectorSize, buffer, offset, nRead);
	  }
	  for (; nRead + Disk.SectorSize <= length; nRead += Disk.SectorSize) {
		  int secn = inode.getSector(pos + nRead);
		  Machine.synchDisk().readSector(secn, buffer, nRead + offset);
	  }
	  if (nRead < length) {
		  int secn = inode.getSector(pos + nRead);
		  byte[] data = new byte[Disk.SectorSize];
		  Machine.synchDisk().readSector(secn, data, 0);
		  System.arraycopy(data, 0, buffer, nRead + offset, length - nRead);
		  nRead = length;
	  }
	  return nRead;
  }
  
  public int write (int pos, byte[] buffer, int offset, int length)
  {
	  if (pos + length > inode.file_size)
		  inode.setFileSize(pos + length);
	  int nWrite = 0;
	  if (pos % Disk.SectorSize != 0) {
		  int secn = inode.getSector(pos);
		  byte[] data = new byte[Disk.SectorSize];
		  Machine.synchDisk().readSector(secn, data, 0);
		  nWrite = Math.min(length, Disk.SectorSize - pos % Disk.SectorSize);
		  System.arraycopy(buffer, offset, data, pos % Disk.SectorSize, nWrite);
		  Machine.synchDisk().writeSector(secn, data, 0);	
	  }
	  for (; nWrite + Disk.SectorSize <= length; nWrite += Disk.SectorSize) {
		  int secn = inode.getSector(pos + nWrite);
		  Machine.synchDisk().writeSector(secn, buffer, nWrite + offset);
	  }
	  if (nWrite < length) {
		  int secn = inode.getSector(pos + nWrite);
		  byte[] data = new byte[Disk.SectorSize];
		  Machine.synchDisk().readSector(secn, data, 0);
		  System.arraycopy(buffer, nWrite + offset, data, 0, length - nWrite);
		  Machine.synchDisk().writeSector(secn, data, 0);		
		  nWrite = length;
	  }
	  return nWrite;
  }
  
  public void create() {
	  inode.link_count = 1;
  }
}
