package nachos.filesys;

import nachos.machine.Lib;

public class SymLink extends File
{
	private String path = null;
  
  public SymLink (INode inode)
  {
    super(inode);
  }
  
  public void create() {
	  super.create();
	  inode.file_type = INode.TYPE_SYMLINK;
	  inode.setFileSize(4);
  }
  
  public void setPath(String p) {
	  path = p;
  }
  
  public String getPath() {
	  return path;
  }
  
  public void save ()
  {
	  /*
	  byte[] data = new byte[4];
	  Lib.bytesFromInt(data, 0, ptr);
	  inode.saveContent(data);
	  */
	  //TODO
  }
  
  public void load ()
  {
	  /*
	  byte[] data = inode.loadContent();
	  ptr = Lib.bytesToInt(data, 0);
	  */
	  //TODO
  }

}
